import { OrderBookHelper } from "../classes";

const EXAMPLE_ORDERS = [[32000, 100], [30000, 200], [31000, 50]];
const EXAMPLE_PRICES = ['32000', '30000', '31000'];

describe('OrderBookHelper.initOrderBook()', () => {
  test('buy side - order book keys populate with order prices', () => {
    const orderBook = OrderBookHelper.initOrderBook([...EXAMPLE_ORDERS], 'buySide');
    const orderBookKeys = Object.keys(orderBook);
    expect(orderBookKeys).toEqual(['30000', '31000', '32000']);
  });

  test('buy side - expect running total of final price to be 350', () => {
    const orderBook = OrderBookHelper.initOrderBook([...EXAMPLE_ORDERS], 'buySide');
    const orderBookKeys = Object.keys(orderBook);
    const { total } = orderBook[orderBookKeys[orderBookKeys.length - 1]];
    expect(total).toEqual(350);
  });

  test('sell side - order book keys populate with order prices', () => {
    const orderBook = OrderBookHelper.initOrderBook([...EXAMPLE_ORDERS], 'sellSide');
    const orderBookKeys = Object.keys(orderBook);
    expect(orderBookKeys).toEqual(['30000', '31000', '32000']);
  });

  test('sell side - expect running total of first price to be 350', () => {
    const orderBook = OrderBookHelper.initOrderBook([...EXAMPLE_ORDERS], 'sellSide');
    const orderBookKeys = Object.keys(orderBook);
    const { total } = orderBook[orderBookKeys[0]];
    expect(total).toEqual(350);
  });
});

describe('OrderBookHelper.incrementOrderBook()', () => {
  test('increments running total by 100', () => {
    const orderBook = OrderBookHelper.initOrderBook([...EXAMPLE_ORDERS], 'buySide');
    const newOrderBook = OrderBookHelper.incrementOrderBook(orderBook, [[30000, 300]], 'buySide');
    const orderBookKeys = Object.keys(newOrderBook);
    const { total } = newOrderBook[orderBookKeys[orderBookKeys.length - 1]];
    expect(newOrderBook['30000'].size).toEqual(300);
    expect(total).toEqual(450);
  });

  test('removes 0 size orders', () => {
    const orderBook = OrderBookHelper.initOrderBook([...EXAMPLE_ORDERS], 'buySide');
    const newOrderBook = OrderBookHelper.incrementOrderBook(orderBook, [[30000, 0]], 'buySide');
    const orderBookKeys = Object.keys(newOrderBook);
    const { total } = newOrderBook[orderBookKeys[orderBookKeys.length - 1]];
    expect(newOrderBook['30000']).toBeUndefined();
    expect(total).toEqual(150);
  });
});

describe('OrderBookHelper.level()', () => {
  test('limits number of keys in order book', () => {
    const orderBook = OrderBookHelper.initOrderBook([...EXAMPLE_ORDERS], 'buySide');
    const newOrderBook = OrderBookHelper.level(orderBook, 1, 'buySide');
    expect(Object.keys(newOrderBook).length).toBe(1);
  });

  test('total equal to size when only one record', () => {
    const orderBook = OrderBookHelper.initOrderBook([...EXAMPLE_ORDERS], 'buySide');
    const newOrderBook = OrderBookHelper.level(orderBook, 1, 'buySide');
    expect(newOrderBook['30000'].size).toEqual(newOrderBook['30000'].total);
  });
});

describe('OrderBookHelper.group()', () => {
  test('group orders to nearest 2500', () => {
    const orderBook = OrderBookHelper.initOrderBook([...EXAMPLE_ORDERS], 'buySide');
    const groupedBook = OrderBookHelper.group(orderBook, 2500, 'buySide');
    expect(Object.keys(groupedBook).length).toBe(2);
    expect(groupedBook['30000']).toBeDefined();
    expect(groupedBook['30000'].size).toEqual(250);
    expect(groupedBook['30000'].total).toEqual(250);

    expect(groupedBook['32500']).toBeDefined();
    expect(groupedBook['32500'].size).toEqual(100);
    expect(groupedBook['32500'].total).toEqual(350);
  });
});

describe('OrderBookHelper.levelAndGroup()', () => {
  test('group orders to nearest 2500 and return a single level', () => {
    const orderBook = OrderBookHelper.initOrderBook([...EXAMPLE_ORDERS], 'buySide');
    const newOrderBook = OrderBookHelper.levelAndGroup(orderBook, 1, 2500, [OrderBookHelper.XBT], 'buySide');

    expect(Object.keys(newOrderBook).length).toBe(1);
    expect(newOrderBook['30000']).toBeDefined();
    expect(newOrderBook['30000'].size).toEqual(250);
    expect(newOrderBook['30000'].total).toEqual(250);
    expect(newOrderBook['30000'].size).toEqual(newOrderBook['30000'].total);
  });
});

describe('OrderBookHelper.sortOrders()', () => {
  test('buy side sorts ascending', () => {
    const sortedOrders = [...EXAMPLE_ORDERS]
      .sort((a, b) => OrderBookHelper.sortOrders(a, b, 'buySide'));
    expect(sortedOrders).toEqual([[30000, 200], [31000, 50], [32000, 100]]);
  });

  test('sell side sorts descending', () => {
    const sortedOrders = [...EXAMPLE_ORDERS]
      .sort((a, b) => OrderBookHelper.sortOrders(a, b, 'sellSide'));
    expect(sortedOrders).toEqual([[32000, 100], [31000, 50], [30000, 200]]);
  });
});

describe('OrderBookHelper.sortPrices()', () => {
  test('buy side sorts ascending', () => {
    const sortedOrders = [...EXAMPLE_PRICES]
      .sort((a, b) => OrderBookHelper.sortPrices(a, b, 'buySide'));
    expect(sortedOrders).toEqual(['30000', '31000', '32000']);
  });

  test('sell side sorts ascending', () => {
    const sortedOrders = [...EXAMPLE_PRICES]
      .sort((a, b) => OrderBookHelper.sortPrices(a, b, 'sellSide'));
    expect(sortedOrders).toEqual(['32000', '31000', '30000']);
  });
});
