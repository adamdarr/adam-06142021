import { renderHook } from "@testing-library/react-hooks";
import { useOrderBook } from "../hooks";
import { WebSocket } from 'mock-socket';
import { OrderBookHelper } from "../classes";

global.WebSocket = WebSocket;

describe('useOrderBook()', () => {
  test('defines socket and handles initial state', async () => {
    const { rerender, result } = renderHook(() => useOrderBook({
      productIds: [OrderBookHelper.XBT]
    }));
    rerender();

    const { socket, buySide, sellSide } = result?.current || {};
    expect(socket).toBeDefined();
    expect(buySide).toEqual({});
    expect(sellSide).toEqual({});
  });
});
