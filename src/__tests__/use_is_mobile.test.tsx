import { renderHook } from "@testing-library/react-hooks";
import { act } from "react-dom/test-utils";
import { useIsMobile } from "../hooks";

describe('useIsMobile()', () => {
  test('defaults to false', () => {
    const { result } = renderHook(() => useIsMobile());
    expect(result?.current?.isMobile).toBeFalsy();
  });

  test('detects mobile width', () => {
    const { result } = renderHook(() => useIsMobile());
    act(() => {
      global.innerWidth = 400;
      global.dispatchEvent(new Event('resize'));
    });
    expect(result?.current?.isMobile).toBeTruthy();
  })
});