import CryptoFacilitiesApi, { SUBSCRIBE_MSG } from "../api/crypto_facilities_api";
import { OrderBookHelper } from "../classes";

const mockSocket: WebSocket = {
  ...new WebSocket(CryptoFacilitiesApi.ADDRESS),
  send: jest.fn(),
};

describe('CryptoFacilitiesApi.send()', () => {
  test('calls internal socket send method', () => {
    CryptoFacilitiesApi.send(mockSocket, SUBSCRIBE_MSG)
    expect(mockSocket.send).toBeCalled();
  });
});

describe('CryptoFacilitiesApi.subscribe()', () => {
  test('calls internal socket send method', () => {
    CryptoFacilitiesApi.subscribe(mockSocket, [OrderBookHelper.XBT]);
    expect(mockSocket.send).toBeCalled();
  });
});

describe('CryptoFacilitiesApi.unsubscribe()', () => {
  test('calls internal socket send method', () => {
    CryptoFacilitiesApi.unsubscribe(mockSocket, [OrderBookHelper.XBT]);
    expect(mockSocket.send).toBeCalled();
  });
});