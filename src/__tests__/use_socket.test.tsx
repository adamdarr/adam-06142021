import { renderHook } from "@testing-library/react-hooks";
import { useSocket } from "../hooks";
import { WebSocket } from 'mock-socket';

global.WebSocket = WebSocket;

const MOCK_ADDRESS = 'wss://localhost:4000/';

describe('useSocket()', () => {
  test('sets socket address', async () => {
    const { rerender, result } = renderHook(() => useSocket({
      address: MOCK_ADDRESS,
    }));
    rerender();
    expect(result?.current?.url).toEqual(MOCK_ADDRESS);
  });
});
