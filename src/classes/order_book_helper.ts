export interface OrderBookEntry {
  price: number;
  size: number;
  total: number;
  depth: number;
}

export type OrderBookHash = { [index: string]: OrderBookEntry }

/**
 * @summary Static class to encapsulate order book logic
 * @constructor
 */
export default class OrderBookHelper {
  static XBT = 'PI_XBTUSD';
  static ETH = 'PI_ETHUSD';
  static XBT_GROUPS = [0.5, 1, 2.5];
  static ETH_GROUPS = [0.05, 0.1, 0.25];
  static DEFAULT_XBT_GROUP = 0.5;
  static DEFAULT_ETH_GROUP = 0.05;

  /**
   * @summary Initialize buy side or sell side state of the order book based on an array of orders.
   */
  static initOrderBook = (orders: number[][], type: 'buySide' | 'sellSide'): OrderBookHash => {
    const orderBook: OrderBookHash = {};
    orders
      .sort((a, b) => OrderBookHelper.sortOrders(a, b, type))
      .forEach((order: number[]) => {
        const [price, size] = order;
        orderBook[`${price}`] = { price, size, total: 0, depth: 0 };
      });
    return OrderBookHelper.addRunningTotalToOrderBook(orderBook, type);
  };

  /**
   * @summary Increment buy side or sell side state of an existing order book based on an array of orders.
   */
  static incrementOrderBook = (orderBook: OrderBookHash, orders: number[][], type: 'buySide' | 'sellSide'): OrderBookHash => {
    let newOrderBook = { ...orderBook };
    orders.forEach((order: number[]) => {
      const [price, size] = order;
      if (size === 0) {
        delete newOrderBook[`${price}`];
      } else {
        newOrderBook[`${price}`] = newOrderBook[`${price}`] ? {
          ...newOrderBook[`${price}`], size, total: 0, depth: 0,
        } : { price, size, total: 0, depth: 0 };
      }
    });
    return OrderBookHelper.addRunningTotalToOrderBook(newOrderBook, type);
  };

  /**
   * @summary Calculate buy side or sell side running total and depth of an order book
   */
  static addRunningTotalToOrderBook = (orderBook: OrderBookHash, type: 'buySide' | 'sellSide') => {
    const sortedOrderBook = OrderBookHelper.sortOrderBook(orderBook, type);
    const sortedPrices = Object.keys(sortedOrderBook)
      .sort((a, b) => OrderBookHelper.sortPrices(a, b, type));

    // Calculate running total
    let runningSize = 0;
    sortedPrices.forEach((price: string) => {
      const { size } = sortedOrderBook[price];
      runningSize += size;
      sortedOrderBook[price] = { ...sortedOrderBook[price], total: runningSize, depth: 0 };
    });

    // Calculate depth
    const maxTotal = sortedOrderBook[sortedPrices[sortedPrices.length - 1]]?.total;
    if (maxTotal) {
      sortedPrices.forEach((price: string) => {
        const { total } = sortedOrderBook[price];
        const depth = total / maxTotal;
        sortedOrderBook[price] = { ...sortedOrderBook[price], depth };
      });
    }
    return sortedOrderBook;
  };

  /**
    * @summary Limit the order book to a specified number of levels
    */
  static level = (orderBook: OrderBookHash, numLevels: number, type: 'buySide' | 'sellSide') => {
    const sortedPrices = Object.keys(orderBook)
      .sort((a, b) => OrderBookHelper.sortPrices(a, b, type))
      .slice(0, numLevels);
    const newOrderBook: OrderBookHash = {};
    sortedPrices.forEach(price => newOrderBook[price] = orderBook[price])
    return OrderBookHelper.addRunningTotalToOrderBook(newOrderBook, type);
  }

  /**
  * @summary Group the order book prices by passed group increment size
  */
  static group = (orderBook: OrderBookHash, group: number, type: 'buySide' | 'sellSide') => {
    const sortedPrices = Object.keys(orderBook)
      .sort((a, b) => OrderBookHelper.sortPrices(a, b, type))
    const newOrderBook: OrderBookHash = {};
    sortedPrices.forEach(price => {
      const groupedPrice = Math.round(Number(price) / group) * group;
      if (!newOrderBook[groupedPrice]) {
        newOrderBook[groupedPrice] = { ...orderBook[price], price: groupedPrice };
      } else {
        newOrderBook[groupedPrice].size += orderBook[price].size;
        newOrderBook[groupedPrice].total += orderBook[price].total;
      }
    });
    return OrderBookHelper.addRunningTotalToOrderBook(newOrderBook, type);
  }

  /**
  * @summary Level and group an order book. Combines calls from level() and 
  * group() functions into a single convenience method.
  */
  static levelAndGroup = (
    orderBook: OrderBookHash,
    numLevels: number,
    group: number,
    productIds: string[],
    type: 'buySide' | 'sellSide'
  ) => {
    let newOrderBook: OrderBookHash = { ...orderBook };
    const [productId] = productIds;
    const isDefault = (productId === OrderBookHelper.XBT && group === OrderBookHelper.DEFAULT_XBT_GROUP) ||
      (productId === OrderBookHelper.ETH && group === OrderBookHelper.DEFAULT_ETH_GROUP)
    if (!isDefault) {
      newOrderBook = OrderBookHelper.group(newOrderBook, group, type);
    }
    newOrderBook = OrderBookHelper.level(newOrderBook, numLevels, type);
    return newOrderBook;
  }

  /**
  * @summary Sort the order book hash based on type
  */
  static sortOrderBook = (orderBook: OrderBookHash, type: 'buySide' | 'sellSide') => {
    const sortedOrderBook: OrderBookHash = {};
    Object.keys(orderBook)
      .sort((a, b) => OrderBookHelper.sortPrices(a, b, type))
      .forEach(k => sortedOrderBook[k] = orderBook[k]);
    return sortedOrderBook;
  }

  /**
  * @summary Compare two orders and sort based on buy side / sell side
  */
  static sortOrders = (a: number[], b: number[], type: 'buySide' | 'sellSide') => {
    return type === 'buySide' ? Number(a[0]) - Number(b[0]) : Number(b[0]) - Number(a[0]);
  }

  /**
  * @summary Compare two prices strings and sort based on buy side / sell side
  */
  static sortPrices = (a: string, b: string, type: 'buySide' | 'sellSide') => {
    return type === 'buySide' ? Number(a) - Number(b) : Number(b) - Number(a);
  }
}
