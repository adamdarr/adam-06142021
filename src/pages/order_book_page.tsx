import React from 'react';
import { OrderBookContainer } from '../containers';

/**
 * @summary Page for order book application.
 * Handle any query params or routing variables.
 * @constructor
 */
const OrderBookPage = () => {
  return <OrderBookContainer />;
};

export default OrderBookPage;
