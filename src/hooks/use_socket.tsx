import React, { useEffect, useRef, useState } from 'react';

export interface UseSocket {
  address: string;
  onOpen?: (socket: WebSocket | null) => void;
  onClose?: (socket: WebSocket | null) => void;
  onMessage?: (socket: WebSocket | null, event: MessageEvent<string>) => void;
  connected?: boolean;
}

/**
 * @summary Hook to connect to a socket and handle sending / receiving messages
 * using the connected socket.
 */
const useSocket = ({ address, onOpen, onClose, onMessage, connected = true }: UseSocket) => {
  const [message, setMessage] = useState<MessageEvent<string> | null>(null);
  const socket: React.MutableRefObject<WebSocket | null> = useRef(null);

  useEffect(() => {
    try {
      if (connected) { // connect to web socket
        socket.current = new WebSocket(address);
        socket.current.onopen = () => onOpen ? onOpen(socket.current) : undefined;
        socket.current.onclose = () => onClose ? onClose(socket.current) : undefined;
        socket.current.onmessage = (event) => setMessage(event);
      } else { // throw error to disconnect
        throw new Error('Disconnected from address');
      }

      return () => { // clean-up socket
        socket.current?.close();
      };
    } catch (e) { // display error / clean-up socket
      console.warn(e?.message);
      socket.current?.close();
    }
  }, [address, connected]);

  useEffect(() => { // handle message state changes
    if (message && onMessage) onMessage(socket?.current, message);
  }, [message]);

  return socket?.current;
};

export default useSocket;
