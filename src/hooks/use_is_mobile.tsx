import { useEffect, useState } from 'react';

/**
 * @summary Detect mobile viewing mode based on screen size.
 * Listen to loads and resizes with event listener.
 */
const useIsMobile = () => {
  const [isMobile, setIsMobile] = useState(false);
  
  const checkIsMobile = () => {
    if (window.innerWidth <= 768) {
      setIsMobile(true);
    } else {
      setIsMobile(false);
    }
  };

  useEffect(() => {
    window.addEventListener('load', checkIsMobile);
    window.addEventListener('DOMContentLoaded', checkIsMobile);
    window.addEventListener('resize', checkIsMobile);

    checkIsMobile();

    return () => { // clean-up listeners
      window.removeEventListener('load', checkIsMobile);
      window.removeEventListener('DOMContentLoaded', checkIsMobile);
      window.removeEventListener('resize', checkIsMobile);
    };    
  });

  return { isMobile };
}

export default useIsMobile;
