import { useCallback, useEffect, useState } from 'react';
import CryptoFacilitiesApi, { ResponseMessage } from '../api/crypto_facilities_api';
import OrderBookHelper, { OrderBookHash } from '../classes/order_book_helper';
import useSocket from './use_socket';
import _debounce from 'lodash.debounce';

const FEED_SNAPSHOT = 'book_ui_1_snapshot';
const DEFAULT_LEVELS = 15;
const DEBOUNCE_TIME = 15;

export interface UseOrderBook {
  productIds: string[];
  group?: number;
  connected?: boolean;
}

/**
 * @summary Hook to connect to order book socket and handle 
 * the state of the order book as the order book feed changes.
 */
const useOrderBook = ({ productIds, group = 0, connected = true }: UseOrderBook) => {
  // hold base state as well as any grouped state
  const [buySide, setBuySide] = useState<OrderBookHash>({});
  const [sellSide, setSellSide] = useState<OrderBookHash>({});
  const [groupedBuySide, setGroupedBuySide] = useState<OrderBookHash>({});
  const [groupedSellSide, setGroupedSellSide] = useState<OrderBookHash>({});

  // debounce grouped state for rendering performance
  const setGroupedBuySideDebounced = useCallback(_debounce(setGroupedBuySide, DEBOUNCE_TIME), []);
  const setGroupedSellSideDebounced = useCallback(_debounce(setGroupedSellSide, DEBOUNCE_TIME), []);
  useEffect(() => {
    setGroupedBuySideDebounced(OrderBookHelper.levelAndGroup(buySide, DEFAULT_LEVELS, group, productIds, 'buySide'));
    setGroupedSellSideDebounced(OrderBookHelper.levelAndGroup(sellSide, DEFAULT_LEVELS, group, productIds, 'sellSide'));
  }, [buySide, sellSide]);

  // connect to crypto facilities api socket
  const socket = useSocket({
    connected,
    address: CryptoFacilitiesApi.ADDRESS,
    onOpen: (socket) => CryptoFacilitiesApi.subscribe(socket, productIds),
    onClose: (socket) => CryptoFacilitiesApi.unsubscribe(socket, productIds),
    onMessage: (_socket, event) => {
      const message: ResponseMessage = event?.data ? JSON.parse(event?.data) : {};
      const { feed, bids, asks } = message;
      if (bids && asks) {
        if (feed === FEED_SNAPSHOT) { // reset state
          const buys = OrderBookHelper.initOrderBook(bids, 'buySide');
          const sells = OrderBookHelper.initOrderBook(asks, 'sellSide');

          setBuySide(buys);
          setSellSide(sells);
        } else { // increment state
          const buys = OrderBookHelper.incrementOrderBook(buySide, bids, 'buySide');
          const sells = OrderBookHelper.incrementOrderBook(sellSide, asks, 'sellSide');

          setBuySide(buys);
          setSellSide(sells);
        }
      }
    },
  });

  // Subscribe to new feed when product IDs change
  useEffect(() => {
    CryptoFacilitiesApi.subscribe(socket, productIds);
  }, [socket, productIds]);

  // return socket and grouped data
  return { socket, buySide: groupedBuySide, sellSide: groupedSellSide };
};

export default useOrderBook;
