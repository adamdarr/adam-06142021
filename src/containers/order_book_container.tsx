import React, { useState } from 'react';
import { CryptoFacilitiesApi } from '../api';
import { OrderBookHelper } from '../classes';
import OrderBook from '../components/order_book';
import { useOrderBook, useIsMobile } from '../hooks';

/**
 * @summary Container to hold state for the order book
 * @constructor
 */
const OrderBookContainer = () => {
  const [connected, setConnected] = useState<boolean>(true);
  const [productIds, setProductIds] = useState<string[]>([OrderBookHelper.XBT]);
  const [group, setGroup] = useState<number>(OrderBookHelper.DEFAULT_XBT_GROUP);
  const { socket, buySide, sellSide } = useOrderBook({ productIds, group, connected });
  const { isMobile } = useIsMobile();

  return (
    <OrderBook
      productIds={productIds}
      buySide={buySide}
      sellSide={sellSide}
      toggleFeed={() => {
        CryptoFacilitiesApi.unsubscribe(socket, productIds);
        const [productId] = productIds;
        if (productId === OrderBookHelper.XBT) {
          setProductIds([OrderBookHelper.ETH]);
          setGroup(OrderBookHelper.DEFAULT_ETH_GROUP);
        } else {
          setProductIds([OrderBookHelper.XBT]);
          setGroup(OrderBookHelper.DEFAULT_XBT_GROUP);
        }
      }}
      killFeed={() => setConnected(!connected)}
      onGroupChange={(g: number) => setGroup(g)}
      isMobile={isMobile}
    />
  );
};

export default OrderBookContainer;
