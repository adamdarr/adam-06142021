export interface RequestMessage {
  event: string;
  feed: string;
  product_ids: string[];
}

export interface ResponseMessage {
  numLevels?: number;
  feed?: string;
  product_id?: string;
  bids?: number[][];
  asks?: number[][];
}

export const FEED = 'book_ui_1';
export const SUBSCRIBE_MSG: RequestMessage = { event: 'subscribe', feed: FEED, product_ids: [] };
export const UNSUBSCRIBE_MSG: RequestMessage = { event: 'unsubscribe', feed: FEED, product_ids: [] };

/**
 * @summary Static class to encapsulate crypto facilities API
 * @constructor
 */
export default class CryptoFacilitiesApi {
  static ADDRESS = 'wss://www.cryptofacilities.com/ws/v1';

  // send subscription message with provided product IDs
  static subscribe = (socket: WebSocket | null, productIds: string[]) => {
    CryptoFacilitiesApi.send(socket, { ...SUBSCRIBE_MSG, product_ids: productIds })
  }

  // send unsubscribe message with provided product IDs
  static unsubscribe = (socket: WebSocket | null, productIds: string[]) => {
    CryptoFacilitiesApi.send(socket, { ...UNSUBSCRIBE_MSG, product_ids: productIds })
  }

  // send any arbitrary request message
  static send = (socket: WebSocket | null, message: RequestMessage) => {
    if (socket?.readyState === socket?.OPEN) {
      socket?.send(JSON.stringify(message));
    }
  }
}