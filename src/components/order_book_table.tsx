import React from 'react';
import { OrderBookHash } from '../classes/order_book_helper';
import { priceFormatter, numberFormatter } from '../utils/format';

export interface OrderBookTableProps {
  type: 'buySide' | 'sellSide';
  orders: OrderBookHash;
  inverted?: boolean;
  sort?: 'asc' | 'desc';
  priceColor?: React.CSSProperties['color'];
}

/**
 * @summary Calculate linear gradient for table row based on depth
 */
const depthBackground = (depth: number, type: 'buySide' | 'sellSide', sort: 'asc' | 'desc') => {
  const to = type === 'buySide' && sort !== 'desc' ? 'right' : 'left';
  const color = type === 'buySide' ? 'rgb(28, 109, 71, 0.5)' : 'rgb(177, 1, 1, 0.5)';
  const percentage = (depth * 100).toFixed(2);
  return `linear-gradient(to ${to}, ${color} ${percentage}%, rgb(20, 18, 31) 0%)`;
};

/**
 * @summary Table to display order book entries and render depth bars
 * @constructor
 */
const OrderBookTable = ({ type, orders, priceColor, inverted = false, sort = 'asc' }: OrderBookTableProps) => {
  const rows = Object.values(orders)
    .sort((a, b) => sort === 'asc' ? a.total - b.total : b.total - a.total)
    .map((order) => {
      const { price, size, total, depth } = order;

      const priceCell = (
        <td style={{
          ...styles.cell,
          color: priceColor || '#21c271',
        }}>
          {priceFormatter.format(price)}
        </td>
      );
      const sizeCell = (
        <td style={styles.cell}>
          {numberFormatter.format(size)}
        </td>
      );
      const totalCell = (
        <td style={styles.cell}>
          {numberFormatter.format(total)}
        </td>
      );

      const rowStyle = {
        ...styles.row,
        background: depthBackground(depth, type, sort),
      };

      return (
        <tr
          key={`${type}${price}`}
          style={rowStyle}
          className="order-book-table-row"
        >
          {inverted ? totalCell : priceCell}
          {sizeCell}
          {inverted ? priceCell : totalCell}
        </tr>
      )
    });

  const priceHeader = <th style={styles.cell}>PRICE</th>;
  const sizeHeader = <th style={styles.cell}>SIZE</th>;
  const totalHeader = <th style={styles.cell}>TOTAL</th>;

  return (
    <table width="100%" style={styles.table}>
      <thead style={styles.header}>
        <tr>
          {inverted ? totalHeader : priceHeader}
          {sizeHeader}
          {inverted ? priceHeader : totalHeader}
        </tr>
      </thead>

      <tbody>
        {rows}
      </tbody>
    </table>
  )
};

const styles: { [index: string]: React.CSSProperties } = {
  table: {
    width: '100%',
    borderCollapse: 'collapse',
  },
  cell: {
    width: '33.33%',
    textAlign: 'right',
    height: 23,
    margin: 0,
    borderStyle: 'hidden',
  },
  row: {
    fontFamily: 'monospace, sans-serif',
  },
  header: {
    fontFamily: 'system-ui',
    fontSize: 14,
    color: '#7b7b7ba8',
  }
}

export default OrderBookTable;
