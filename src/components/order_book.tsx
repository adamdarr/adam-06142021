import React from 'react';
import OrderBookHelper, { OrderBookHash } from '../classes/order_book_helper';
import OrderBookTable from './order_book_table';
import { BsArrowLeftRight, BsExclamationCircle } from "react-icons/bs";

export interface OrderBookProps {
  productIds: string[];
  buySide: OrderBookHash;
  sellSide: OrderBookHash;
  toggleFeed: () => void;
  killFeed: () => void;
  onGroupChange: (group: number) => void;
  isMobile?: boolean;
}

/**
 * @summary Layout and render order book header, filters, tables, charts, butons, etc.
 * @constructor
 */
const OrderBook = ({
  productIds,
  buySide,
  sellSide,
  toggleFeed,
  killFeed,
  onGroupChange,
  isMobile = false,
}: OrderBookProps) => {
  const [productId] = productIds;
  let groups = [];
  if (productId === OrderBookHelper.XBT) {
    groups = [...OrderBookHelper.XBT_GROUPS];
  } else {
    groups = [...OrderBookHelper.ETH_GROUPS];
  }

  const groupOptions = groups.map((g) => (
    <option key={`group_${g}`} value={g}>Group {g.toFixed(2)}</option>
  ));

  return (
    <>
      <div style={styles.orderBookDashboard}>
        <div style={styles.orderBookSection}>
          <div style={styles.orderBookHeader}>Order Book</div>
          <select
            className="order-book-app-select"
            style={styles.select}
            onChange={(e) => {
              const value = e?.target?.value;
              if (value) {
                onGroupChange(Number(value));
              }
            }}
          >
            {groupOptions}
          </select>
        </div>

        <div style={styles.orderBookTableSection}>
          <div
            style={styles.orderBookTableDiv}
            className="order-book-table"
          >
            {isMobile ? (
              <OrderBookTable
                type="buySide"
                orders={buySide}
                sort="desc"
              />
            ) : (
              <OrderBookTable
                type="sellSide"
                orders={sellSide}
                inverted
              />
            )}
          </div>
          <div
            style={styles.orderBookTableDiv}
            className="order-book-table"
          >
            {isMobile ? (
              <OrderBookTable
                type="sellSide"
                orders={sellSide}
                priceColor="#ed0c0c"
              />
            ) : (
              <OrderBookTable
                type="buySide"
                orders={buySide}
              />
            )}
          </div>
        </div>
      </div>

      <div style={styles.buttonGroup}>
        <button style={styles.toggleButton} onClick={toggleFeed}>
          <BsArrowLeftRight style={{ verticalAlign: -2 }} /> Toggle Feed
        </button>

        <button style={styles.killButton} onClick={killFeed}>
          <BsExclamationCircle style={{ verticalAlign: -2 }} /> Kill Feed
        </button>
      </div>
    </>
  );
}

const styles: { [index: string]: React.CSSProperties } = {
  orderBookHeader: {
    padding: 12,
    display: 'inline-block',
  },
  orderBookSection: {
    backgroundColor: '#14121f',
    filter: 'drop-shadow(0px 0px 1px black)',
    marginBottom: 5,
  },
  orderBookTableSection: {
    backgroundColor: '#14121f',
    filter: 'drop-shadow(0px 0px 1px black)',
    marginBottom: 5,
  },
  orderBookDashboard: {
    marginBottom: 15,
  },
  buttonGroup: {
    textAlign: 'center',
    marginBottom: 10,
  },
  toggleButton: {
    color: 'white',
    backgroundColor: 'rgb(81 39 183)',
    cursor: 'pointer',
    border: 'none',
    borderRadius: 3,
    padding: 8,
    marginRight: 10,
  },
  killButton: {
    color: 'white',
    backgroundColor: '#b10101',
    cursor: 'pointer',
    border: 'none',
    borderRadius: 3,
    padding: 8,
  },
  select: {
    marginTop: 9,
    height: 25,
    display: 'inline-block',
    float: 'right',
    marginRight: 10,
    appearance: 'none',
    WebkitAppearance: 'none',
    padding: 5,
    borderRadius: 5,
    backgroundColor: 'rgb(64 65 68)',
    color: 'white',
    border: 'unset',
  }
}

export default OrderBook;
