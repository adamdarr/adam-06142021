import React from 'react';
import './App.css';
import { OrderBookPage } from './pages';

/**
 * @summary Top-level app with single page for order book
 * @constructor
 */
const App = () => {
  return (
    <div className="order-book-app">
      <OrderBookPage />
    </div>
  );
}

export default App;
