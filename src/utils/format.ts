const priceFormat = {
  currency: "USD",
  minimumFractionDigits: 2,
  currencyDisplay: "symbol",
};

const numberFormat = {
  minimumFractionDigits: 0,
  currencyDisplay: "symbol",
};

const priceFormatter = new Intl.NumberFormat("en-US", priceFormat);
const numberFormatter = new Intl.NumberFormat("en-US", numberFormat);

export { priceFormatter, numberFormatter };
